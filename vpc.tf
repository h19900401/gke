variable "project_id" {
  description = "project_id"
}

variable "region" {
  description = "region"
}

variable "zone" {
  description = "zone"
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

module myip {
  source  = "4ops/myip/http"
  version = "1.0.0"
}

# VPC
resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
}

# Subnet
resource "google_compute_subnetwork" "subnet" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.10.0.0/24"
}

resource "google_compute_firewall" "allow-localIP" {
  name    = "allowlocalip"
  network = google_compute_network.vpc.name

  allow {
    protocol = "all"
  }

  source_ranges = [module.myip.address]
}

resource "google_compute_firewall" "allow-ingress-from-iap" {
  name    = "allow-ingress-from-iap"
  network = google_compute_network.vpc.name

  allow {
    protocol = "tcp"
    ports = [ "22" ]
  }

  source_ranges = ["35.235.240.0/20"]
}